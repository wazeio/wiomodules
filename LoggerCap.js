/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 (Seafnox) Kirill Warp
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

function Logger(level){
  this.level = level || "Debug";
  this.useFile = false;
  this.useConsole = true;
  this.fs = require("fs");
  return this;
}

Logger.prototype.setLevel = function(level){
  this.level = level;
};

Logger.prototype.attachFile = function(path) {
  try {
    this.useFile = true;
    var _path = path.substr(0, path.lastIndexOf("/"));
    var filename = path.substr(path.lastIndexOf("/") + 1);
    if (filename.length === 0) {
      filename = "log.log";
    }
    if (typeof _path !== 'undefined' && !this.fs.existsSync(_path)) {
      this.fs.mkdirSync(_path);
    }
    this.fd = this.fs.openSync(_path + "/" + filename, "a+");
  } catch (e) {
    this.useFile = false;
    this.fatal("Can't create or use file", path);
    this.fatal(e.stack);
  }
};

Logger.prototype.disableConsole = function(disable) {
  this.useConsole = disable;
};

Logger.prototype.levels = ['Fatal', 'Error', 'Warn', 'Info', 'Debug', 'Trace'];

Logger.prototype.buildMsg = function() {
  var date = new Date();
  var time = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
  var args = [];
  for (var i = 0; i < arguments[0].length; i++) {
    if (typeof arguments[0][i] === 'object') {
      try {
        args.push(JSON.stringify(arguments[0][i]));
      } catch (e) {
        if (args instanceof Array) {
          args.push("Array is too big: " + arguments[0][i].length);
        } else {
          args.push("Object is too big: " + Object.keys(arguments[0][i]).join(","));
        }
        args.push(e.stack);
      }
    } else {
      args.push(arguments[0][i]);
    }
  }
  return time + " " + args.join(", ");
};

Logger.prototype.checkAndSend = function(checkedLevel, args, consoleLevel) {
  if (this.levels.indexOf(checkedLevel) <= this.levels.indexOf(this.level)) {
    if (this.useConsole) {
      if (consoleLevel === 'Error') {
        console.error(this.buildMsg(args));
      } else {
        console.log(this.buildMsg(args));
      }
      if (this.useFile) {
        this.fs.write(this.fd, checkedLevel + " " + this.buildMsg(args) + "\n");
      }
    }
  }
};

Logger.prototype.fatal = function() { this.checkAndSend('Fatal', arguments, 'Error')};

Logger.prototype.error = function() { this.checkAndSend('Error', arguments, 'Error')};

Logger.prototype.warn = function() { this.checkAndSend('Warn', arguments, 'Error')};

Logger.prototype.info = function() { this.checkAndSend('Info', arguments)};

Logger.prototype.debug = function() { this.checkAndSend('Debug', arguments)};

Logger.prototype.trace = function() { this.checkAndSend('Trace', arguments)};

if (module.parent){
  module.exports = Logger;
}
else {
  var logger = new Logger();
  logger.fatal('Fatal');
  logger.error('Error');
  logger.warn('Warn');
  logger.info('Info');
  logger.debug('Debug');
  logger.trace('Trace');
}