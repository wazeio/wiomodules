/**
 * Created by Asondo on 11.04.2016.
 */
var socketConnector = function(url, callback) {
    var socket;
    var that;
    var status;
    var msgQueue = [];
    var tagList = [];

    function reinit() {
        console.warn('Reconnect to WS');
        that.initSocket();
        that.checkMsgQueue();
    }
    function checkMsgQueue() {
        if (msgQueue.length > 0 && status != undefined && status == "open") {
            while (msgQueue.length > 0) {
                var msg = msgQueue.shift();
                socket.send(msg);
            }
        }
    }
    function onMessageHandler(message) {
        var json = JSON.parse(message);
        Object.keys(tagList).forEach(function (tag) {
            if (json.tag.toString().indexOf(tag) !== -1) {
                tagList[tag].forEach(function (callback) {
                    callback(json);
                });
            }
        });
    }

    return {
        initSocket: function() {
            that = this;
            socket = new WebSocket(url);
            socket.onopen = function () {
                console.log('Connected to WS');
                status = 'open';
                if (callback) {
                    callback();
                }

            };
            socket.onclose = function (event) {
                console.warn('Соединение закрыто. Код: ' + event.code + ' причина: ' + event.reason);
                status = "close";
                if (!event.wasClean) {
                    // обрыв соединения - ждем 5 сек и реконектимся (запускаем сами себя)
                    setTimeout(reinit, 5000);
                }
            };
            socket.onmessage = function (event) {
                onMessageHandler(event.data);
            };
            socket.onerror = function (error) {
                //ошибочка вышла - коннект нам не светит
                console.error("Ошибка " + error.message);
                status = "error";
                setTimeout(reinit, 5000);
            };
        },
        subscribe: function(tag, callback) {
            if (tag != undefined && typeof tag === "string" && callback != undefined && typeof callback === "function") {
                if (tagList[tag] == undefined) {
                    tagList[tag] = [];
                }
                tagList[tag].push(callback);
            }
        },
        unsubscribe: function(tag, callback) {
            if (tag != undefined && typeof tag === "string" && tagList[tag] != undefined) {
                tagList[tag] = tagList[tag].filter(item => item == callback ? null : item);
                return tagList[tag].length;
            }
            return 0;
        },
        send: function(message) {
            switch (message.action) {
                case "subscribe":
                    that.subscribe(message.tag, message.callback);
                    delete message.callback;
                    break;
                case "unsubscribe":
                    var subscribed = that.unsubscribe(message.tag, message.callback);
                    if (subscribed > 0) {
                        return;
                    }
                    break;
            }
            msgQueue.push(JSON.stringify(message));
            checkMsgQueue();
        }
    }
};

module.exports = socketConnector;
