/*
 * wioBusConnector
 *   Модуль предназначен для маршрутизации сообщений
 *   между подписчиками с автоматическим переподключением
 *   и очередью сообщений (FIFO), что гарантирует доставку всех сообщений.
 *
 *   Позволяет создавать:
 *
 *     * Сервер-маршрутизатор (router) для
 *       реализации функционала подписок, отписок и маршрутизации
 *       сообщений между подписчиками.
 *
 *     * Клиент-подписчик (client) для
 *       подключения к роутеру или серверу;
 *       дает возможность подписываться на теги
 *       и отписываться от них, публиковать сообщения
 *       сразу по группе тегов
 *
 *     * Сервер-слушатель (server) для прямого подключения
 *       клиентов и передачи данных. Не имеет возможности
 *       реализовать подписки и рассылки, в остальном
 *       идентичен маршрутизатору
 */
'use strict';
try {
    var Logger = require('./LoggerCap.js');
} catch(err) {
    throw new Error('Can not find LoggerCap.js');
}
var debug = true;
var logger = new Logger('Info');

process.on('uncaughtException', function(err) {
    logger.error('Caught exception: ' + err.stack);
    if (err.stack.indexOf("ECONNREFUSED") === -1
        && err.stack.indexOf("ECONNRESET") === -1) {
        process.exit(1);
    }
});

exports.router = (function() {

    try {
        var websocketServer = require('nodejs-websocket');
        websocketServer.setMaxBufferLength(9000000);
    } catch(err) {
        logger.error('Can not find module websocket', err);
        throw new Error('Can not find module websocket');
    }

    var socket;
    var clients = {};
    var messagePull = {};

    var userEventHandlers = {};
    var connectionCounter = 0;

    function Client(connection, id) {

        this.id = id;
        this.messageId = 0;
        this.tags = [];
        this.connection = connection;

    }

    Client.prototype.subscribed = function(tags) {
        var subs = this.tags;
        for (var i=0; i<subs.length; i++) {
            if (tags.indexOf(subs[i]) > -1) return true;
        }
        return false;
    };

    Client.prototype.subscribe = function(tag) {
        this.tags.push(tag);
    };

    Client.prototype.unsubscribe = function(tag) {
        var subs = this.tags;
        for (var i=0; i<subs.length; i++) {
            if (tag.indexOf(subs[i])+1) {
                subs.splice(i, 1); // Удаляем тег из списка подписок
                return;
            }
        }
    };

    Client.prototype.send = function(tag, data, timestamp) {

        var messageId = this.id + '_' + this.messageId++;
        var tmstmp = timestamp ? timestamp : Date.now();

        try {
            var message = JSON.stringify({tag:tag, data:data, id: messageId, timestamp: tmstmp});
        } catch(err) {
            logger.error('Error: JSON can not stringify with recursive links');
            throw new Error('JSON can not stringify with recursive links');
        }

        if (this.connection.readyState === 1) this.connection.sendText(message);
        else logger.error('Send at state: ' + this.connection.readyState);
    };

    Client.prototype.response = function(id) {
        var message = JSON.stringify({action:'response', id:id});
        this.connection.sendText(message);
    };

    function requestEventHandler(connection) {
        var id = connectionCounter++;
        // Запоминаем данные подключения для этого клиента
        clients[id] = new Client(connection, id);
        // Присваиваем персональный идентификатор этому клиенту
        var message = JSON.stringify({action:'setId', id:id});
        connection.sendText(message);
        // Задаем обработчики событий
        connection.on('text', messageEventHandler);
        connection.on('close', closeEventHandler);
        // Пришло новое сообщение от клиента
        function messageEventHandler(message) {

            var json = JSON.parse(message);
            // Отправляем подтверждение для этого сообщения
            clients[id].response(json.id);
            // Если такого сообщения еще не было
            if (!messagePull[json.id]) {
                // Фиксируем сообщение с автоматическим удалением через минуту
                messagePull[json.id] = setTimeout(function(messages, id){
                    delete messages[id]; // Удаляем сообщение
                }, 60000, messagePull, json.id);
            } else { // Если дубль
                return; // Игнорируем
            }

            if (json.action === 'sub') { // Клиент хочет подписаться
                // Запоминаем подписку на тег для этого клиента
                // Сразу проверяем не подписан ли он уже
                if (!clients[id].subscribed(json.tag)) clients[id].subscribe(json.tag);
                logger.debug('Client ' + id + ' want to subscribe to tag: ' + json.tag);
            } else if (json.action === 'unsub') { // Клиент хочет отписаться
                // Удаляем подписку на тег для этого клиента
                clients[id].unsubscribe(json.tag);
                logger.debug('Client ' +  id + ' want to unsubscribe from tag: ' + json.tag);
            } else { // Пришло обычное сообщение
                // Передаем в callback тег, сообщение и отправителя
                if (userEventHandlers.message) userEventHandlers.message(json.tag, json.data, clients[id], json.timestamp);
                logger.debug('Client ' + id + ' want to publish message: ' + json.data + ' with tag: ' + json.tag);
            }

        }

        function closeEventHandler(reasonCode, description) {

            delete clients[id];
            logger.warn('Client ' + id + ' disconnected.');
            if (userEventHandlers.close) userEventHandlers.close(reasonCode, description, id);
            else {
                logger.warn('No handler for close event');
            }
        }

        // Передаем данные подключения с методами в callback
        if (userEventHandlers.request) userEventHandlers.request(clients[id]);
        else {
            logger.error('No handler for request event');
        }
    }

    return {

        start: function(port, callback) {
            socket = websocketServer.createServer(requestEventHandler);
            socket.listen(port);
            if (callback && socket) {
                callback();
            }
        },

        on: function (event, callback) {
            userEventHandlers[event] = callback;
        },

        publish: function(tag, data, timestamp) {

            logger.debug('Publish message to subscribers: ' + tag, data);
            // Проходим по списку подключенных
            for (let client in clients) {
                if (clients.hasOwnProperty(client)) {
                    // Если клиент подписан на этот тег
                    if (clients[client].subscribed(tag)) {
                        // Отправляем ему сообщение
                        clients[client].send(tag, data, timestamp);
                    }
                }
            }
        }
    }
})();
exports.client = (function() {

    try {
        var websocketClient = require('nodejs-websocket');
        websocketClient.setMaxBufferLength(9000000);
    } catch(err) {
        logger.error('Can not find module websocket', err);
        throw new Error('Can not find module websocket');
    }

    var socket;
    var host;
    var port;
    var pid = 0; // Персональный id в системе для маркировки сообщений

    var userEventHandlers = {};
    // FIFO буфер сообщений
    // Отправка по одному сообщению из очереди
    var messagePull = [];
    var messageId = 0;
    var tagList = [];

    var timeout;
    var reconnectTimeout;
    var autoReconnect;
    var connecting;
    // Запрещает дублирование подключения при автоматическом переподключении
    var double = 0;
    // Таймер для тестов
    var timer = {

        object: {},
        start: function(name) {

            timer.object[name] = Date.now();

        },
        end: function(name) {

            var ms = (Date.now() - timer.object[name]);
            delete timer.object[name];
            return ms;

        }

    };
    // Обработчик событий по подпискам
    var busConnector;
    var subscribesHandler;

    function connectEventHandler() {
        logger.debug("connectEventHandler socket.readyState", socket.readyState);
        if (socket.readyState === 1) {
            logger.info('Connected to socket-server on port ' + port);
        } else {
            logger.info('Connect event. Current status: ' + socket.readyState);
        }
        // Снимаем флаг
        connecting = false;
        logger.info("connectEventHandler Resubscribe", tagList.length);
        for (let i = 0; i < tagList.length; i++) {
            logger.info('Resubscribe: ' + tagList[i]);
            putMessage({action: 'sub', tag: tagList[i]});
        }
        // Отправляем сообщения из пула, если они там есть
        sendNext(true); // true - hard mode (resending)
    }
    function messageEventHandler(data) {

        var message = JSON.parse(data);
        logger.debug("message", message.action, message.id, "pull size", messagePull.length);
        if (message.action === 'setId') {

            pid = message.id;
            logger.debug('Get a personal id');
            // Событие о подключении срабатывает только после получения pid
            if (userEventHandlers.connect) {
                // Передаем событие в пользовательский callback
                userEventHandlers.connect(false, pid);
                userEventHandlers.connect = null;
            } else {
                logger.error('No handler for connect event');
            }

        } else if (message.action === 'response') {

            if (messagePull[0]) {
                // Если подтверждение не от этого сообщения
                if (messagePull[0].id !== message.id) {
                    return logger.warn('Not for current message response. Last id: '+messagePull[0].id+', respnse for: '+message.id);
                }
                if (debug) {
                    var time = 0;
                    var timeInQueue = 0;
                    if (messagePull[0].action === 'sub') timeInQueue = timer.end('subscribeAtQueue');
                    else {
                        if (messagePull[0].action === 'unsub') timeInQueue = timer.end('unsubscribeAtQueue');
                        else timeInQueue = timer.end('messagesAtQueue');
                    }
                    if (messagePull[0].action === 'sub') time = timer.end('subscribe');
                    else {
                        if (messagePull[0].action === 'unsub') time = timer.end('unsubscribe');
                        else time = timer.end('message');
                    }
                    if (userEventHandlers.timer) {
                        userEventHandlers.timer(time, messagePull[0].action, timeInQueue);
                    }
                }
                // Удаляем таймер timeout и сообщение
                clearTimeout(messagePull[0].timeout);
                messagePull.splice(0, 1);
                // Отправляем следующее сообщение из пула если есть
                sendNext();

            } else {
                logger.error('Unknown response from socket-server for message id: ' + message.id);
            }

        } else {
            if (!userEventHandlers.message) return logger.error('No handler for message event');
            // Передаем событие в пользовательский callback
            userEventHandlers.message(message);
        }

    }
    function closeEventHandler(reasonCode, description) {
        // Пытаемся переподключится самостоятельно, если нужно
        reconnect();
        if (userEventHandlers.close) {
            // Передаем событие в пользовательский callback
            userEventHandlers.close(reasonCode, description);
        } else {
            logger.warn('No handler for close event');
        }
    }
    function errorEventHandler(error) {
        if (userEventHandlers.error) {
            // Передаем событие в пользовательский callback
            userEventHandlers.error(error);
        } else {
            logger.warn('No handler for error event.', error.stack);
        }
    }
    function connectionStatusHandler() {
        if (socket.readyState !== 1) {
            logger.error('Error: no response from server, timeout');
            reconnect();
        }
    }
    function reconnect(hardMode) {
        if (!autoReconnect) {
            if (userEventHandlers.error) userEventHandlers.error('Error: connection lost. Auto-reconnect disabled.');
            else {
                logger.error('Connection lost. Error: no handler for error event.');
            }
            return;
        }
        logger.debug("reconnect1 socket.readyState", socket.readyState);
        logger.info('Connection closed. Try to reconnect to ' + host + ':' + port);
        // Если уже переподключаемся, то выходим отсюда
        if (!hardMode) {
            if (connecting) return;
            connecting = true;
        } else {
            if (userEventHandlers.close) userEventHandlers.close('Sending message timeout: no response from server. Try to re-connect.');
            else {
                logger.warn('Sending message timeout. No handler for close event');
            }
        }
        socket = websocketClient.connect('ws://'+host+':'+port+'/', connectEventHandler);
        socket.on('text', messageEventHandler);
        socket.once('close', closeEventHandler);
        socket.on('error', errorEventHandler);
        logger.debug("reconnect2 socket.readyState", socket.readyState);
        setTimeout(function() {
            if (connecting) {
                logger.error('Connection error: timeout');
                reconnect(true);
            }
        }, reconnectTimeout); // Проверяем состояние подключения через некоторое время

    }
    function sendNext(repeat) {
        // Если нечего передавать выходим
        if (!messagePull[0]) return;
        // Если не заведомо повторная передача
        if (!repeat) {
            // Если это сообщение в процессе отправки выходим
            if (messagePull[0].sending) return;
        }
        // Если ранее был выставлен таймер, удаляем его
        if (messagePull[0].timeout) {
            clearTimeout(messagePull[0].timeout);
            delete messagePull[0].timeout;
        }
        // Фиксируем начало работы с этим сообщением
        messagePull[0].sending = true;
        // Отправляем
        try {
            var message = JSON.stringify(messagePull[0]);
        } catch(err) {
            logger.error('Error: JSON can not stringify with recursive links');
            throw new Error('JSON can not stringify with recursive links');
        }
        if (debug) {
            if (messagePull[0].action === 'sub') timer.start('subscribe');
            else if (messagePull[0].action === 'unsub') timer.start('unsubscribe');
            else timer.start('message');
        }

        if (socket.readyState === 1) socket.sendText(message);
        else logger.error('Error: Send at state: ' + socket.readyState);
        // Если не пришло подтверждение - переподключаемся
        //messagePull[0].timeout = setTimeout(reconnect, timeout);
    }
    function send(object, timestamp) {
        putMessage(object, timestamp);
        // Пытаемся отправить
        sendNext();
    }
    function putMessage(object, timestamp) {
        // Даем уникальный id этому сообщению
        object.id = pid + '_' + messageId++;
        object.timestamp = timestamp ? timestamp : Date.now(); // Added
        // Отправляем сообщение в пул
        messagePull.push(object);
    }
    function subscribe(tag) {
        if (debug) {
            timer.start('subscribeAtQueue');
        }
        if (tagList.indexOf(tag) === -1) {
            tagList.push(tag);
        }
        logger.info("subscribeAtQueue", tag);
        send({action: 'sub', tag: tag});
    }
    function unsubscribe(tag) {
        if (debug) {
            timer.start('unsubscribeAtQueue');
        }
        tagList = tagList.filter(function(_tag) {return _tag === tag ? null : _tag});
        logger.info("unsubscribeAtQueue", tag);
        send({action: 'unsub', tag: tag});
    }
    // Работа с подписками
    function subscriberOnMessage(message) {
        var event = message.data;
        var tags = message.tag;

        logger.debug('Receive message from eventBus by tag ' + tags + ': ' + JSON.stringify(event));
        //noinspection JSUnresolvedFunction
        var timestamp = message.timestamp;
        // Превращаем теги в массив если они в виде строки
        if (typeof message.tag === 'string') {
            tags = message.tag.split(/[\s,]/);
        }
        logger.debug('Get tags: ' + JSON.stringify(tags));
        // Если теги в виде массива
        if (tags instanceof Array) {
            for (let i=0; i<tags.length; i++) {
                let tag = tags[i];
                // Запускаем обработчик на каждый тег, если существует
                if (subscribesHandler[tag]) {
                    subscribesHandler[tag](event, timestamp);
                }
            }
        }
    }
    // Публичные свойства и методы
    return {
        on: function(event, callback) {
            userEventHandlers[event] = callback;
        },
        subscribe: function(tag) {
            subscribe(tag);
        },
        unsubscribe: function(tag) {
            unsubscribe(tag);
        },
        publish: function(tag, data, timestamp) {
            if (debug) {
                timer.start('messagesAtQueue');
            }
            send({tag: tag, data: data}, timestamp);
        },
        connect: function(options, callback) {
            busConnector = this;
            host = options.host || '127.0.0.1';
            port = options.port || 5000;
            timeout = options.timeout || 5000;
            reconnectTimeout = options.reconnectTimeout || 10000;
            autoReconnect = options.autoReconnect || true;
            userEventHandlers.connect = callback;
            if (double && autoReconnect) {
                logger.error('Duplicate connection to socket server with auto-reconnect option. Aborted');
                return;
            }
            double = true;
            socket = websocketClient.connect('ws://'+host+':'+port+'/', connectEventHandler);
            socket.on('text', messageEventHandler);
            socket.once('close', closeEventHandler);
            socket.on('error', errorEventHandler);

            // После первого подключения нужно проверить состояние через некоторое время
            setTimeout(connectionStatusHandler, timeout);
        },
        send: function(tag, data, timestamp) {
            send({tag: tag, data: data}, timestamp);
        },
        set subscriber(messageHandlers) {
            subscribesHandler = messageHandlers;
            for (let tag in messageHandlers) {
                //noinspection JSUnresolvedFunction,JSUnfilteredForInLoop
                busConnector.subscribe(tag);
            }
            busConnector.on('message', subscriberOnMessage);
        }
    }

})();
exports.server = (function() {

    try {
        var websocketServer = require('nodejs-websocket');
    } catch(err) {
        logger.error('Can not find module websocket', err);
        throw new Error('Can not find module websocket');
    }

    var socket;
    var clients = {};
    var messagePull = {};

    var userEventHandlers = {};
    var connectionCounter = 0;

    function Client(connection, id) {
        this.id = id;
        this.messageId = 0;
        this.tags = [];
        this.connection = connection;
    }

    Client.prototype.send = function(tag, data) {
        var messageId = this.id + '_' + this.messageId++;
        try{
            var message = JSON.stringify({tag:tag, data:data, id: messageId, timestamp:Date.now()});
        } catch(err) {
            logger.error('Error: JSON can not stringify with recursive links');
            throw new Error('JSON can not stringify with recursive links');
        }
        if (this.connection.readyState === 1) this.connection.sendText(message);
        else logger.error('Send at state: ' + this.connection.readyState);
    };

    Client.prototype.response = function(id) {
        var message = JSON.stringify({action:'response', id:id});
        this.connection.sendText(message);
    };

    function requestEventHandler(connection) {

        var id = connectionCounter++;
        // Запоминаем данные подключения для этого клиента
        clients[id] = new Client(connection, id);
        // Присваиваем персональный идентификатор этому клиенту
        var message = JSON.stringify({action:'setId', id:id});
        connection.sendText(message);
        // Задаем обработчики событий
        connection.on('text', messageEventHandler);
        connection.on('close', closeEventHandler);

        // Пришло новое сообщение от клиента
        function messageEventHandler(message) {
            var json = JSON.parse(message);
            // Отправляем подтверждение для этого сообщения
            clients[id].response(json.id);
            // Если такого сообщения еще не было
            if (!messagePull[json.id]) {
                // Фиксируем сообщение с автоматическим удалением через минуту
                messagePull[json.id] = setTimeout(function(messages, id){
                    delete messages[id]; // Удаляем сообщение
                }, 60000, messagePull, json.id);
            } else { // Если дубль
                return; // Игнорируем
            }
            // Передаем в callback тег, сообщение и отправителя
            if (userEventHandlers.message) userEventHandlers.message(json.tag, json.data, clients[id], json.timestamp);
        }

        function closeEventHandler(reasonCode, description) {
            delete clients[id];
            logger.warn('Client ' + id + ' disconnected.');
            if (userEventHandlers.close) userEventHandlers.close(reasonCode, description, id);
        }

        // Передаем данные подключения с методами в callback
        if (userEventHandlers.request) userEventHandlers.request(clients[id]);

    }

    return {
        start: function(port,callback) {
            socket = websocketServer.createServer(requestEventHandler).listen(port);
            if (callback && socket) {
                callback();
            }
        },

        on: function (event, callback) {
            userEventHandlers[event] = callback;
        }

    }

})();