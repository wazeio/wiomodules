'use strict';

var mongoClient = require('mongodb').MongoClient;
var mongoObject = require('mongoobject');

var logger;
var config;
var db;
var debug = false;

exports.init = function(configuration, loggerCap, callback) {
    return new Promise((resolve, reject) => {

        // Проверяем наличие конфигурации
        if (configuration) config = configuration;
        else {
            reject('Error: need config data');
            if (callback && typeof(callback) === 'function') {

                callback('Error: need config data');

            }
            return;
        }
        // Проверяем наличие логгера
        if (logger) {
            logger = loggerCap;
        } else {
            logger = {
                error: fireError,
                warn: fireError,
                debug: debugMsg
            }
        }
        // Пробуем сгенерировать url для подключения к БД
        try {
            var url = 'mongodb://' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.path;
        } catch(e) {
            reject(e);
            if (callback && typeof(callback) === 'function') {
                callback(e);
            }
        }
        // Подключаемся
        connect(url, (err, _db) => {

            if (err) {
                logger.error('Connection to MongoDB failed', err);
                reject(err);
                return;
            }

            if (logger.info) logger.info('Connected correctly to server');
            else console.log('Connected correctly to server');

            db = _db;
            resolve(true);
            if (callback && typeof(callback) === 'function') {
                callback(false, true);
            }
        });
    });
};
exports.checkTables = function (tables, callback) {
    let counter = 0;
    let config = tables ? tables : config.mongodb.tables;
    let length = config.length;
    return new Promise((resolve, reject) => {
        for (let i=0; i<config.length; i++) {
            checkDBTable(config[i]).then(()=>{
                counter++;
                // Когда отработали все checkDBTable
                if (counter === length) {
                    resolve(true);
                    // Если callback есть
                    if (callback && typeof(callback) === 'function') {

                        callback(true);

                    }
                }
            },
            (err) => {

                reject(err);

            });
        }
    });
};
exports.insert = function (table, _dataObject, callback) {
    var dataObject;
    if (!(_dataObject instanceof Array)) {
        dataObject = [_dataObject];
    } else {
        dataObject = _dataObject;
    }
    return new Promise((resolve, reject) => {
        exports.insertManyIntoDB(table, dataObject, callback)
        .then((result)=>{
            resolve(result);
        }, (err) => {
            reject(err);
        });
    });
};
exports.insertMany = function (table, dataObjects, callback) {
    return new Promise((resolve, reject) => {
        try {
            var collection = db.collection(table);
            collection.insertMany(dataObjects, function (err, result) {

                if (err) {

                    logger.error('insert into ' + table + ' failed', err);
                    reject(err);

                } else {

                    logger.debug('insert into ' + table + ' success', result.length);
                    resolve(result);

                }

                // Если callback есть
                if (callback && typeof(callback) === 'function') {

                    callback(err, result);

                }


            });
        } catch (e) {
            logger.error('db collection ' + table + ' failed', e);
            reject(e);
        }
    });
};
exports.update = function (table, whereFilter, newDataFields, callback) {
    return new Promise((resolve, reject) => {
        try {
            var collection = db.collection(table);
            collection.updateMany(whereFilter, {$set: newDataFields}, function (err, result) {

                if (err) {

                    logger.error('update into ' + table + ' failed ' + JSON.stringify(whereFilter), err);
                    reject(err);

                } else {

                    logger.debug('update into ' + table + ' success', result.length);
                    resolve(result);

                }

                if (callback && typeof(callback) === 'function') {

                    callback(err, result);

                }

            });
        } catch (e) {

            logger.error('db collection ' + table + ' failed', e);
            reject(e);

        }
    });
};
exports.updateOrInsert = function (table, whereFilter, dataObject, callback) {
    return new Promise((resolve, reject) => {
        try {
            var collection = db.collection(table);
            collection.updateMany(whereFilter, {$set: dataObject}, {upsert: true}, function (err, result) {
                if (err) {
                    logger.error('update or insert into ' + table + ' failed ' + JSON.stringify(whereFilter) + ' ' + JSON.stringify(dataObject), err);
                    reject(err);
                } else {
                    logger.debug('update or insert into ' + table + ' success', result.result.n);
                    resolve(result);
                }
                if (callback && typeof(callback) === 'function') {
                    callback(err, result);
                }
            });
        }
        catch (e) {
            logger.error('db collection ' + table + ' failed', e);
            reject(e);
        }
    });
};
exports.multiUpdate = function (table, whereFilter, dataObject, callback) {
    return new Promise((resolve, reject) => {
        try {
            var collection = db.collection(table);
            collection.updateMany(whereFilter, {$set: dataObject}, {multi: true}, function (err, result) {
                if (err) {
                    logger.error('update or insert into ' + table + ' failed ' + JSON.stringify(whereFilter) + ' ' + JSON.stringify(dataObject), err);
                    reject(err);
                } else {
                    logger.debug('update or insert into ' + table + ' success', result.result.n);
                    resolve(result);
                }
                if (callback && typeof(callback) === 'function') {
                    callback(err, result);
                }
            });
        }
        catch (e) {
            logger.error('db collection ' + table + ' failed', e);
            reject(e);
        }
    });
};
exports.pureUpsert = function (table, whereFilter, dataObject, callback) {
    return new Promise((resolve, reject) => {
        try {
            var collection = db.collection(table);
            collection.updateMany(whereFilter, dataObject, {upsert: true}, function (err, result) {
                if (err) {
                    logger.error('update or insert into ' + table + ' failed ' + JSON.stringify(whereFilter) + ' ' + JSON.stringify(dataObject), err);
                    reject(err);
                } else {
                    logger.debug('update or insert into ' + table + ' success', result.result.n);
                    resolve(result);
                }
                if (callback && typeof(callback) === 'function') {
                    callback(err, result);
                }
            });
        }
        catch (e) {
            logger.error('db collection ' + table + ' failed', e);
            reject(e);
        }
    });
};
exports.delete = function (table, whereFilter, callback) {
    return new Promise((resolve, reject) => {
        try {
            var collection = db.collection(table);
            collection.deleteMany(whereFilter, function (err, result) {

                if (err) {

                    logger.error('delete from ' + table + ' failed ' + JSON.stringify(whereFilter), err);
                    reject(err);

                } else {

                    logger.debug('delete from ' + table + ' success', result.length);
                    resolve(result);

                }

                if (callback && typeof(callback) === 'function') {
                    callback(err, result);
                }

            });
        } catch (e) {
            logger.error('db collection ' + table + ' failed', e);
            reject(e);
        }
    });
};
exports.get = function (table, options, callback, limitPerOnce) {
    return new Promise((resolve, reject) => {

        try {

            var _limitPerOnce = limitPerOnce || 1000;
            var pipe = [];
            
            if (typeof options.columns !== 'undefined') {
                if (options.columns instanceof Array) {
                    var columns = {};
                    for (var i=0; i<options.columns.length; i++) {
                        columns[options.columns[i]] = 1;
                    }
                    options.columns = columns;
                }
                pipe.push({$project: options.columns});
            }
            if (typeof options.match !== 'undefined') {
                pipe.push({$match: options.match});
            }
            if (typeof options.where !== 'undefined') {
                pipe.push({$match: options.where});
            }
            if (typeof options.sort !== 'undefined') {
                pipe.push({$sort: options.sort});
            }
            if (typeof options.order !== 'undefined') {
                pipe.push({$sort: options.order});
            }
            if (typeof options.group !== 'undefined') {
                pipe.push({$group: options.group});
            }
            if (typeof options.having !== 'undefined') {
                pipe.push({$match: options.having});
            }

            var limit;
            var skip;
            if (_limitPerOnce){
                if (options.limit instanceof Array) {

                    limit = options.limit[0];
                    skip = options.limit[1];

                } else {

                    limit = options.limit || _limitPerOnce;
                    skip = 0;

                }
                options.limit = [limit, skip + _limitPerOnce];
                if (limit > _limitPerOnce) {
                    limit = _limitPerOnce;
                }
                pipe.push({$skip: skip});
                pipe.push({$limit: limit});
            }
            var collection = db.collection(table);
            collection.aggregate(pipe, {allowDiskUse: true}, function (err, result) {
                if (err) {
                    logger.error('get from ' + table + ' failed ' + JSON.stringify(pipe), err);
                    reject(err);
                } else {
                    logger.debug('get from ' + table + ' success', result.length);
                    resolve(result);
                }
                if (callback && typeof(callback) === 'function') {
                    callback(err, result);
                }
            });
        } catch (e) {
            logger.error('db collection ' + table + ' failed', e);
            reject(e);
        }
    });
};

exports.escape = mongoObject.escape;
exports.timedate = function (timestamp) {
    var date = new Date();
    var result = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' 00:00:00';
    if (typeof timestamp !== 'undefined') {
        date = new Date(timestamp);
        result = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    }
    return new Date(result).getTime();
};

function connect(url, callback){
    mongoClient.connect(url, function (err, _db) {
        if (err) {
            logger.warn('MongoDB connection error: ' + err + ' (Try to reconnect after 1 second)');
            setTimeout(connect, 1000, url, callback);
            return;
        }
        if (callback && typeof(callback) === 'function') {
            callback(err, _db);
        }
    });

}
function checkDBTable(table) {
    return new Promise((resolve, reject) => {
        db.createCollection(table, function (err, collection) {
            if (err) {
                logger.error('Creating connection ' + table + ' failed', err);
                reject(err);
                return;
            }
            logger.debug('Creating connection ' + table + ' successed', collection);
            resolve(true);
        });
    });
}
function getFromDB(table, options, callback, limitPerOnce) {
    if (!callback || typeof(callback) !== 'function') {
        return logger.error(new Error("Callback is not defined").stack);
    }
    try {
        var _limitPerOnce = limitPerOnce || 1000;
        var pipe = [];
        
        if (typeof options.columns !== 'undefined') {
            if (options.columns instanceof Array) {
                var columns = {};
                for (var i=0; i<options.columns.length; i++) {
                    columns[options.columns[i]] = 1;
                }
                options.columns = columns;
            }
            pipe.push({$project: options.columns});
        }
        if (typeof options.match !== 'undefined') {
            pipe.push({$match: options.match});
        }
        if (typeof options.where !== 'undefined') {
            pipe.push({$match: options.where});
        }
        if (typeof options.sort !== 'undefined') {
            pipe.push({$sort: options.sort});
        }
        if (typeof options.order !== 'undefined') {
            pipe.push({$sort: options.order});
        }
        if (typeof options.group !== 'undefined') {
            pipe.push({$group: options.group});
        }
        if (typeof options.having !== 'undefined') {
            pipe.push({$match: options.having});
        }

        var limit;
        var skip;
        if (_limitPerOnce){
            if (options.limit instanceof Array) {
                limit = options.limit[0];
                skip = options.limit[1];
            } else {
                limit = options.limit || _limitPerOnce;
                skip = 0;
            }
            options.limit = [limit - _limitPerOnce, skip + _limitPerOnce];
            if (limit > _limitPerOnce) {
                limit = _limitPerOnce;
            }
            pipe.push({$skip: skip});
            pipe.push({$limit: limit});
        }
        var collection = db.collection(table);

        collection.aggregate(pipe, {allowDiskUse: true}, function (err, ownResult) {
            if (err) {
                logger.error('get from ' + table + ' failed ' + JSON.stringify(pipe), err);
                callback(err);
            } else {
                if (_limitPerOnce){
                    if (ownResult.length === _limitPerOnce) {
                        getFromDB(table, options, function (err, result) {
                            if (err) {
                                logger.error('get from ' + table + ' failed ' + JSON.stringify(pipe), err);
                                callback(err);
                            } else {
                                ownResult.concat(result);
                                callback(null, ownResult);
                            }
                        }, _limitPerOnce);
                        return logger.debug('get from ' + table + ' step', ownResult.length);
                    }
                }
                logger.debug('get from ' + table + ' success', ownResult.length);
                callback(null, ownResult);
            }
        });

    } catch (err) {
        logger.error('db collection ' + table + ' failed', e);
        callback(err);
    }
}
function fireError(errorType, error) {
    var stack;
    if (error) {
        stack = error.stack;
    }
    var date = new Date();
    var time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
    console.error(time + ' ' + errorType + ': ' + stack);

}
function debugMsg() {
    if (debug) {
        var date = new Date();
        var time = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        var args = [];
        for (var i = 0; i < arguments.length; i++) {
            args.push(arguments[i]);
        }
        console.log(time + ' ' + args.join(', '));
    }
}
